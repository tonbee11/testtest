import React from 'react'
import { Container,Row,Col,Image,Card } from 'react-bootstrap';
import Button from '@mui/material/Button';
import './Portfolio.css'
import Pic1 from '../Pic/user.jpg';
import Skill1 from '../Pic/skill1.jpg';
import TimelineCom from '../component/timeline';
import Iframe from 'react-iframe';
import Typewriter from "typewriter-effect";
import NavScrollExample from '../component/navbar';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import { SocialIcon } from 'react-social-icons';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import CallIcon from '@mui/icons-material/Call';
import PhpPic from "../Pic/php.png"
import ReactPic from "../Pic/react.png"
import HTMLPic from "../Pic/html.png"
import NodePic from "../Pic/node.svg"
import MysqlPic from "../Pic/mysql.png"
import MongodbPic from "../Pic/mongodb.png"
import MediaControlCard from '../component/card';
function  Portfolio ()  {
    const skillData = [
        {
            language : "PHP",
            time : "3",
            pic : PhpPic,
            detail : "Development includes the creation of a functional and visually "
        },
        {
            language : "ReactJS",
            time : "2",
            pic : ReactPic,
            detail : "Development includes the creation of a functional and visually appealing website, as well as the development of any necessary digital assets. Whatever you need, I can help bring your vision to life."
        },
        {
            language : "NodeJS",
            time : "1",
            pic : NodePic,
            detail : "Development includes the creation of a functional and visually appealing website, as well as the development of any necessary digital assets. Whatever you need, I can help bring your vision to life."
        },
        {
            language : "HTML/CSS",
            time : "3",
            pic : HTMLPic,
            detail : "Development includes the creation of a functional and visually appealing website, as well as the development of any necessary digital assets. Whatever you need, I can help bring your vision to life."
        },
        {
            language : "MYSQL",
            time : "2",
            pic : MysqlPic,
            detail : "Development includes the creation of a functional and visually appealing website, as well as the development of any necessary digital assets. Whatever you need, I can help bring your vision to life."
        },
        {
            language : "MongoDB",
            time : "1",
            pic : MongodbPic,
            detail : "Development includes the creation of a functional and visually appealing website, as well as the development of any necessary digital assets. Whatever you need, I can help bring your vision to life."
        }
    ]
    const data_work = [
        {
            time : "01/2018",
            icon : <FastfoodIcon />,
            title : "Wild Blue",
            detail : "Built front-end and back-end websites for promoting medical conferences, registration systems, and payment systems"
        },
        {
            time : "08/2018",
            icon : <FastfoodIcon />,
            title : "SSUP",
            detail : "Responsible for back-end website maintenance of CRM"
        },
        {
            time : "02/2021",
            icon : <FastfoodIcon />,
            title : "GIPSIC",
            detail : "Built websites using ReactJS and NodeJS Designed and built a procurement system for Central Pattana Public Company Limitedb Designed solutions for supporting IoT devices"
        }
    ]
    const data_edu = [
        {
            time : "2013",
            icon : <FastfoodIcon />,
            title : "Taweethapisek School",
            detail : "Major: Math-English"
        },
        {
            time : "2016",
            icon : <FastfoodIcon />,
            title : "Bansomdejchaopraya Rajabhat University",
            detail : "Bachelor of Business Administration in Business Computer"
        }
    ]
    return (
        <>
            {/* <NavScrollExample/> */}
            <Row className='hero-section'>
                <Col xs={12} md={6} style={{textAlign : "center"}}>
                <Image
                    className='img-profile'
                     width="600"
                    src={Pic1}
                    roundedCircle
                    />
                </Col>
                <Col xs={12} md={6}>
                    <div className='title'>
                        <h2>Wittawat Sangpong</h2>
                        <div className="type-writer">
                            <Typewriter
                    
                            onInit={(typewriter)=> {
                                
                                typewriter
                                .typeString("I am a digital designer and developer with a passion for helping startups establish a strong online presence. With over 60 projects completed and over 3 years of experience in the industry, I have the skills and expertise to bring your brand to life.")
                                .pauseFor(1000)
                                .start();
                            }}/>
                        </div>
                   </div>
                </Col>
            </Row>
            <Row  className='skill-section'>
                        <Col xs={12} md={12}>
                            <h3> Skill</h3>
                        </Col>
                        {skillData.map((skillDa) => {
                        return <Col xs={6} md={4} style={{display: 'flex', justifyContent: 'center'}}>
                        
                        <MediaControlCard data={skillDa}/>
                        </Col>
                        })}
            </Row>
            <Row className='work-section'>
                <Col xs={12} md={6} className="work-ex">
                    <h3>Work Experience</h3>
                    <TimelineCom data={data_work}/>
                    
                </Col>
                <Col xs={12} md={6} className="work-edu">
                    <h3>Educational </h3>
                    <TimelineCom  data={data_edu}/>
                    
                </Col>
            </Row>
            <Row className='contact-section'>
                <Col xs={12} md={12}>
                    <h3> Contact</h3>
                </Col>
                 <Col xs={12} md={6} >
                    <div className='contact'>
                        <h4>Social</h4>
                        <InstagramIcon/> : tonbee08<br/>
                        <FacebookIcon/> : https://www.facebook.com/Owlsilver<br/>
                        <InstagramIcon/> : tonfai_un<br/>
                        <CallIcon/> : 094-402-7651
                    </div>
                    <div className='contact'>
                        <h4>Address</h4>
                            Ban klang muang kallaprapruk 9/234 soi. Kumnanmaen Bangwa Pasecharoen Bangkok 10160
                    </div>
                 </Col>
                 {/* <Col xs={12} md={4} >
                    <Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2740.9640973328583!2d100.42537786042031!3d13.699750042493081!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e297fb95f03007%3A0x4b4db0574c96d06e!2z4Lir4Lih4Li54LmI4Lia4LmJ4Liy4LiZ4LiB4Lil4Liy4LiH4LmA4Lih4Li34Lit4LiH4LiB4Lix4Lil4Lib4Lie4Lik4LiB4Lip4LmM!5e0!3m2!1sth!2sth!4v1676120961533!5m2!1sth!2sth"
                        width="80%"
                        height="320px"
                        id=""
                        className=""
                        display="block"
                        position="relative"
                    />
                </Col> */}
                <Col xs={12} md={6} >
                    <div className='location'>
                        <h4>Location</h4>
                    </div>
                    <Iframe url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2740.9640973328583!2d100.42537786042031!3d13.699750042493081!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e297fb95f03007%3A0x4b4db0574c96d06e!2z4Lir4Lih4Li54LmI4Lia4LmJ4Liy4LiZ4LiB4Lil4Liy4LiH4LmA4Lih4Li34Lit4LiH4LiB4Lix4Lil4Lib4Lie4Lik4LiB4Lip4LmM!5e0!3m2!1sth!2sth!4v1676120961533!5m2!1sth!2sth"
                        width="80%"
                        height="300px"
                        id=""
                        className="map-test"
                        display="block"
                        position="relative"
                    />
                 </Col>
            </Row>
      </>
      );
    
}
export default Portfolio;
