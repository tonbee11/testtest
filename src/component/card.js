import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';


export default function MediaControlCard(prop) {

  return (
    // <Card sx={{ maxWidth: 345 }} style={{margin : 8 ,backgroundColor: 'transparent',boxShadow: 'none',color: "#fff"}}>
    <Card sx={{ maxWidth: 345 }} style={{padding : 8,margin : 8 ,backgroundColor: '#212529',color: "#fff"}}>
    <CardActionArea>
      <CardMedia
        component="img"
        height="120px"
        sx={{ padding: "1em 1em 0 1em", objectFit: "contain" }}
        // style={{backgroundSize : "contain",marginTop : 8,marginBottom :8}}
        image={prop.data.pic}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
        {prop.data.language}
        </Typography>
        <Typography variant="body2" >
          Lizards are a widespread group of squamate reptiles, with over 6,000
          species, ranging across all continents except Antarctica
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
  );
}