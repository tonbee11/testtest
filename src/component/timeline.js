import * as React from 'react';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import TimelineDot from '@mui/lab/TimelineDot';
import LaptopMacIcon from '@mui/icons-material/LaptopMac';
import HotelIcon from '@mui/icons-material/Hotel';
import RepeatIcon from '@mui/icons-material/Repeat';
import Typography from '@mui/material/Typography';

export default function TimelineCom(prop) {
    // const data = [
    //     {
    //         time : "01/2018",
    //         icon : <FastfoodIcon />,
    //         title : "Wild Blue",
    //         detail : "ทำเว็บงานประชุมเป็น และมีหน้าสำหรับล็อกอิน"
    //     },
    //     {
    //         time : "01/2023",
    //         icon : <FastfoodIcon />,
    //         title : "SSUP",
    //         detail : "ทำงานเป็น CRM ใช้ PHP เป็นหลัก"
    //     },
    //     {
    //         time : "01/2023",
    //         icon : <FastfoodIcon />,
    //         title : "GIPSIC",
    //         detail : "พัฒนาเว็บไซต์ด้วย ReactJs ,NodeJs และอื่นๆ"
    //     }
    // ]
  return (
    <Timeline >
        { prop.data.map((da) =>{
            return  <TimelineItem>
            <TimelineOppositeContent
              sx={{ m: 'auto 0' }}
              align="right"
              variant="body2"
            >
             {da.time}
            </TimelineOppositeContent>
            <TimelineSeparator>
              <TimelineConnector />
              <TimelineDot>
                {da.icon}
              </TimelineDot>
              <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent sx={{ py: '12px', px: 2 }}>
              <Typography variant="h6" component="span">
              {da.title}
              </Typography>
              <Typography> {da.detail}</Typography>
            </TimelineContent>
          </TimelineItem>
        })}
     
    </Timeline>
  );
}